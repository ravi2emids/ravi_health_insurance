package com.healthapp.spring.premium.business;

import org.hibernate.validator.resourceloading.AggregateResourceBundleLocator.AggregateBundle;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.healthapp.spring.premium.beans.Patient;

@Service("patientPremium")
public class PatientPremiumBusiness implements PatientPremium {
	// private static final double BASE_PREMIUM = 5000;

	public PatientPremiumBusiness() {
		System.out.println("Premium initilize");
	}

	@Override
	public double getPatientPremiun(Patient patient) {
		double finalPremium = 5000d;
		System.out.println("Business layer:" + patient);
		System.out.println(patient.getPatientAge());

		if (18 < patient.getPatientAge() && patient.getPatientAge() <= 40) {
			int no = (patient.getPatientAge() - 20) / 5;
			if (patient.getPatientAge() % 5 != 0)
				no = no + 1;
			finalPremium = (finalPremium * Math.pow(1.1, no));

		} else if (patient.getPatientAge() > 40) {
			int no = (patient.getPatientAge() - 40) / 5;
			if (patient.getPatientAge() % 5 != 0)
				no = no + 1;
			finalPremium = (finalPremium * Math.pow(1.2, no));

		}

		if (patient.getPatientGender().equals("M")) {
			finalPremium = (finalPremium + finalPremium * 0.02);

		}

		String[] currentHealth = patient.getCurrentHealth();
		if (currentHealth != null) {
			int size = currentHealth.length;
			finalPremium = (finalPremium * Math.pow(1.01, size));

		}

		String[] habits = patient.getHabits();
		if (habits != null) {
			int count = 0;
			int habitsSize = habits.length;
			for (int i = 0; i < habits.length; i++) {
				if (habits[i].equals("dailyExercise")) {
					if (habitsSize == 1) {
						finalPremium = (finalPremium * Math.pow(0.03, finalPremium));

						habitsSize = 2;
					}
					count++;
				}
			}

			if (count == 1) {
				habitsSize = habitsSize - 2;
				finalPremium = (finalPremium * Math.pow(1.03, habitsSize));

			}
		}
		return finalPremium;

	}

}
