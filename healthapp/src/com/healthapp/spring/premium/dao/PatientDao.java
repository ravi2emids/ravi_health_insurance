package com.healthapp.spring.premium.dao;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.healthapp.spring.premium.beans.Patient;

@Component("patientDao")
public class PatientDao {

	private NamedParameterJdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource jdbcTemplate) {
		this.jdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);

	}

	public PatientDao() {
		System.out.println("Dao loaded");
	}

	public boolean create(Patient patient) {
		BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(patient);

		return jdbcTemplate.update(
				"insert into Patient(patientName,patientGender,patientAge,patientRegisterDate,currentHealth,habits,patientPremium) values(:patientName,:patientGender,:patientAge,:patientRegisterDate,:currentHealth,:habits,:patientPremium)",
				params) == 1;

	}

}
