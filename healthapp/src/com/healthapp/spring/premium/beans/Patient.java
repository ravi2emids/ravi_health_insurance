package com.healthapp.spring.premium.beans;

import java.util.Arrays;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

public class Patient {
	private int patientId;

	@NotNull(message = "Name Should not be empty")
	@Size(min = 5, max = 100, message = "Name must be between 4 to 100 letter")
	private String patientName;
	
	@NotBlank(message="Gender should not be empty")
	private String patientGender;
	
	private int patientAge;

	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@NotBlank(message="Date should not be empty")
	private String patientRegisterDate;
	private String[] currentHealth;
	private String[] habits;

	private double patientPremium;

	public Patient() {
		// TODO Auto-generated constructor stub
	}

	public Patient(int patientId,
			@NotNull(message = "Name Should not be empty") @Size(min = 5, max = 100, message = "Name must be between 5 to 100 letter") String patientName,
			@NotNull(message = "Should not be empty") String patientGender, @NotNull int patientAge,
			String patientRegisterDate, String[] currentHealth, String[] habits, double patientPremium) {

		this.patientId = patientId;
		this.patientName = patientName;
		this.patientGender = patientGender;
		this.patientAge = patientAge;
		this.patientRegisterDate = patientRegisterDate;
		this.currentHealth = currentHealth;
		this.habits = habits;
		this.patientPremium = patientPremium;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientGender() {
		return patientGender;
	}

	public void setPatientGender(String patientGender) {
		this.patientGender = patientGender;
	}

	public int getPatientAge() {
		return patientAge;
	}

	public void setPatientAge(int patientAge) {
		this.patientAge = patientAge;
	}

	public String getPatientRegisterDate() {
		return patientRegisterDate;
	}

	public void setPatientRegisterDate(String patientRegisterDate) {
		this.patientRegisterDate = patientRegisterDate;
	}

	public String[] getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(String[] currentHealth) {
		this.currentHealth = currentHealth;
	}

	public String[] getHabits() {
		return habits;
	}

	public void setHabits(String[] habits) {
		this.habits = habits;
	}

	public double getPatientPremium() {
		return patientPremium;
	}

	public void setPatientPremium(double patientPremium) {
		this.patientPremium = patientPremium;
	}

	@Override
	public String toString() {
		return "Patient [patientName=" + patientName + ", patientGender=" + patientGender + ", patientAge=" + patientAge
				+ ", patientRegisterDate=" + patientRegisterDate + ", currentHealth=" + Arrays.toString(currentHealth)
				+ ", habits=" + Arrays.toString(habits) + ", patientPremium=" + patientPremium + "]";
	}

}
