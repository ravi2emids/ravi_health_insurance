package com.healthapp.spring.premium.beans;

public class CurrentHealth {
	private boolean hyperTension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overWeight;

	public CurrentHealth() {
		// TODO Auto-generated constructor stub
	}

	public CurrentHealth(boolean hyperTension, boolean bloodPressure, boolean bloodSugar, boolean overWeight) {

		this.hyperTension = hyperTension;
		this.bloodPressure = bloodPressure;
		this.bloodSugar = bloodSugar;
		this.overWeight = overWeight;
	}

	public boolean isHyperTension() {
		return hyperTension;
	}

	public void setHyperTension(boolean hyperTension) {
		this.hyperTension = hyperTension;
	}

	public boolean isBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public boolean isBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public boolean isOverWeight() {
		return overWeight;
	}

	public void setOverWeight(boolean overWeight) {
		this.overWeight = overWeight;
	}

	
}
