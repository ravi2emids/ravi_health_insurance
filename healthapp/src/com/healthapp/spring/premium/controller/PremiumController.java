package com.healthapp.spring.premium.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.healthapp.spring.premium.beans.Patient;
import com.healthapp.spring.premium.business.PatientPremium;
import com.healthapp.spring.premium.service.PatientService;

@Controller
public class PremiumController {

	private String name = null;
	private double price = 0.0;
	private PatientService patientService;
	private PatientPremium patientPremium;

	@Autowired
	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	@Autowired
	public void setPatientPremium(PatientPremium patientPremium) {
		this.patientPremium = patientPremium;
	}

	@RequestMapping("/newPremium")
	public String newPremium() {

		return "patient_created";

	}

	@RequestMapping("/createPatient")
	public String createPatient(Model model) {
		// / System.out.println(patient);

		model.addAttribute("patient", new Patient());

		return "newpremium";

	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public String redirect(Model model) {

		model.addAttribute("patientname", name);
		model.addAttribute("finalpremium", price);
		return "success";
	}

	@RequestMapping(value = "/doCreate", method = RequestMethod.POST)
	public String doCreate(Model model, @Valid Patient patient, BindingResult result) {

		if (result.hasErrors()) {
			return "newpremium";
		} else {
			System.out.println("form validated");
		}

		// System.out.println(patient);
		model.addAttribute("pat", patient);

		double pa = patientPremium.getPatientPremiun(patient);
		int g = (int) Math.ceil(pa);
		/*model.addAttribute("patientname", patient.getPatientName());
		model.addAttribute("finalpremium", g);*/
		patient.setPatientPremium(pa);
		name = patient.getPatientName();
		price = g;
		patientService.create(patient);
		// System.out.println("Final premium: "+d);
		// model.addAttribute("finalpremium", );

		return "redirect:/success";

	}
}
