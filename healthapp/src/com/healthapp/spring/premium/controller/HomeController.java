package com.healthapp.spring.premium.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String showHome() {
		return "home";

	}
	
	@RequestMapping("/home")
	public String home() {
		return "home";

	}

}
