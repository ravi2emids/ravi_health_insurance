package com.healthapp.spring.premium.service;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.healthapp.spring.premium.beans.Patient;
import com.healthapp.spring.premium.dao.PatientDao;

@Service("patientService")
public class PatientService {

	private PatientDao patientDao;

	public PatientService() {
		System.out.println("Service init");
	}

	@Autowired
	public void setPatientDao(PatientDao patientDao) {
		this.patientDao = patientDao;
	}

	public void create(Patient patient) {
		System.out.println("Data on Service layer: "+patient);
		patientDao.create(patient);
	}

}
