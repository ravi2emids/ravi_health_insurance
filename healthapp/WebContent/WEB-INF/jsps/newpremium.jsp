<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/style.css">
<title>Insert title here</title>
</head>
<body>
	<nav class="navbar navbar-toggleable-md navbar-inverse bg-faded" style="background-color: #4CAF50;">
	<button class="navbar-toggler navbar-toggler-right" type="button"
		data-toggle="collapse" data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="#">Health Insurance</a>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active"><a class="nav-link" href="${pageContext.request.contextPath}/home">Home
					<span class="sr-only">(current)</span>
			</a></li>

		</ul>
		<!-- <form class="form-inline my-2 my-lg-0">
			<input class="form-control mr-sm-2" type="text" placeholder="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
		</form> -->
	</div>
	</nav>
	<h1 align="center">Patient Registration</h1>
	<marquee>
		<h3>We are here to serve you.</h3>
	</marquee>



	<div class="container">
		<!-- Content here -->



		<div class="card">
			<div class="card-block">


				<sf:form action="${pageContext.request.contextPath}/doCreate"
					method="post" modelAttribute="patient">

					<div class="form-group">
						<sf:label path="patientName">Patient Name</sf:label>
						<sf:input type="text" class="form-control col-10"
							path="patientName"></sf:input>

						<sf:errors path="patientName" cssClass="error" />
					</div>

					<div class="form-group">
						<sf:label path="patientGender">Gender</sf:label>
						<br>
						<sf:radiobutton path="patientGender" value="M" />
						&nbsp Male
						<sf:radiobutton path="patientGender" value="F" />
						&nbsp Female

						<sf:radiobutton path="patientGender" value="others" />
						&nbsp Other
					</div>

					<div class="form-group">
						<sf:label path="patientAge">Age</sf:label>
						<sf:input type="text" class="form-control col-10"
							path="patientAge"></sf:input>

						<sf:errors path="patientAge" cssClass="error" />
					</div>

					<div class="form-group">
						<sf:label path="patientRegisterDate">Date</sf:label>
						<sf:input type="text" class="form-control col-10"
							path="patientRegisterDate"></sf:input>

						<sf:errors path="patientRegisterDate" cssClass="error" />
					</div>

					<div class="form-group">
						<sf:label path="patientRegisterDate">Current Health</sf:label>
						<br>
						<tr>

							<!-- <td>Hyper Tension</td> -->
							<td><sf:checkbox path="currentHealth" value="hyperTension" />&nbspHyper
								Tension</td>
							<br>


						</tr>

						<tr>

							<td></td>
							<td><sf:checkbox path="currentHealth" value="bloodPressure" />&nbspBlood
								Pressure</td>
							<br>

						</tr>

						<tr>

							<td></td>
							<td><sf:checkbox path="currentHealth" value="bloodSugar" />&nbspBlood
								Sugar</td>
							<br>

						</tr>
						<tr>

							<td></td>
							<td><sf:checkbox path="currentHealth" value="overWeight" />&nbspOver
								Weight</td>
							<br>

						</tr>
					</div>



					<div class="form-group">
						<sf:label path="patientRegisterDate">Habits</sf:label>
						<br>
						<tr>


							<td><sf:checkbox path="habits" value="smoking" />&nbspSmoking</td>
							<br>


						</tr>

						<tr>

							<td></td>
							<td><sf:checkbox path="habits" value="alcohol" />&nbspAlcohol</td>
							<br>

						</tr>

						<tr>

							<td></td>
							<td><sf:checkbox path="habits" value="dailyExercise" />&nbspDaily
								Exercise</td>
							<br>

						</tr>
						<tr>

							<td></td>
							<td><sf:checkbox path="habits" value="drugs" />&nbspDrugs</td>
							<br>

						</tr>
					</div>







					<tr>
						<!-- <td><input type="submit" value="Create"></td> -->
						<button type="submit" class="btn btn-primary">Submit</button>
					</tr>







				</sf:form>


			</div>
		</div>



	</div>

	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
		integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
		crossorigin="anonymous"></script>
</body>
</html>